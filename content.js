// content.js

if (window.location.href.indexOf("lot_details") > -1) {
     
	var vatRate = 0.2;
	var defaultBuyersPremium = 0.22;

	var currentBidInput = $('#currentbid');
	var currentBid = parseFloat(currentBidInput.val().substr(1));

	var nextBidInput = $('input[name="minnextbid"]');
	var nextBid = (typeof nextBidInput.val() !== 'undefined') ? parseFloat(nextBidInput.val().substr(1)) : 0;

	var maxBidInput = $('input[name="mybid"]');
	var myMaxBid = (typeof maxBidInput.val() !== 'undefined') ? parseFloat(maxBidInput.val()) : 0;

	var vatApplicable = ($("td font b:contains(Vat:)").parent().text().slice(-1) === 'Y');
	var buyersPremium = Number("0." + ($("td font b:contains(Buyer\'s Premium:)").parent().text().slice(-3).substr(0,2)));


	$(maxBidInput).on('change, keyup', updateTotalBid);
}

function updateTotalBid()
{
	
	if (window.location.href.indexOf("lot_details") === -1) {
		return false;
	}
	
	myMaxBid = (typeof maxBidInput.val() !== 'undefined') ? parseFloat(maxBidInput.val()) : 0;
	console.log(myMaxBid);
	
	let totals = {
		currentBidAmount: calculateTotalBidCost(currentBid),
		minNextBidAmount: calculateTotalBidCost(nextBid),
		myMaxBidAmount: calculateTotalBidCost(myMaxBid)
	};
	setupTooltips(totals);
	console.log(totals);
	
}

function setupTooltips(totals)
{
	$('.jpHelperTooltip').remove();
	
	addTooltip(currentBidInput, totals.currentBidAmount);
	addTooltip(nextBidInput, totals.minNextBidAmount);
	addTooltip(maxBidInput, totals.myMaxBidAmount);
}

function addTooltip(input, amount)
{
	
	if(typeof input.val() === 'undefined' || isNaN(amount)) {
		return false;
	}
	
	let tooltipHtml = "<div class='jpHelperTooltip' style='border:1px solid #e1e1e1;border-radius:4px; background:#fff;'>£" + amount.toFixed(2) + "</div>";
	input.after(tooltipHtml);
}


function calculateTotalBidCost(bidAmount) {
	
	let temp_totalVat = Number(bidAmount) * vatRate;
	let temp_premium = Number(bidAmount) * buyersPremium;
	let temp_premiumVAT =  (vatApplicable) ? (temp_premium * vatRate) : 0;
	
	let totalCost = temp_totalVat + temp_premium + temp_premiumVAT + bidAmount;
	return totalCost;
}
	

updateTotalBid();


chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
    
    }
);