// background.js

var dbName = "todos-vanillajs";



// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
  // Send a message to the active tab
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(
		activeTab.id,
		{
			"message": "clicked_browser_action"
		}
	);
  });
});

chrome.notifications.onButtonClicked.addListener(buttonHandler)


chrome.alarms.onAlarm.addListener(alarmHandler); 


function buttonHandler( notificationId, buttonIndex )
{
	
	if (buttonIndex === 0 ) {
		var newURL = "http://johnpyeauctions.co.uk/";
        chrome.tabs.create({ url: newURL });
	}
	console.log(notificationID, buttonIndex);
}

	
function alarmHandler( alarm )
{
	
	
  chrome.notifications.create("reminder", {
        type: "basic",
		requireInteraction: true,
        iconUrl: "img/icon/icon_128.png",
        title: "Don't forget!",
        message: "Your watched item is ending soon.",
		buttons: [
		{
			title: "View item"
		},
		{
			title: "Dismiss"
		}
		]
     }, function(notificationId) {});
	 
	 
}