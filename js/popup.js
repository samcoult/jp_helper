

$('button#updateTotal').on('click', updateTotalBid);	
$('button#setReminder').on('click', setReminder);	



function setReminder()
{



	let notificationId = "";
	let options = {
		type: "basic",
		title: "This is a notification",
		message: "Hey! This is your notification",
		eventTime: Date.now() + 60,
		priority: 1

	};
	chrome.notifications.update(notificationId, options, reminderCallback);
}

function reminderCallback(id) {
	alert('hit the callback' + ' ' + chrome.runtime.lastError);
}


function updateTotalBid()
{
	
	var vatRate = 0.2;
	var defaultBuyersPremium = 0.22;

	var currentBidInput = $('#bidAmount');
	var currentBid = parseFloat(currentBidInput.val());



	var vatApplicable = ($("#vatApplicable").val() === 'Y');
	var buyersPremium = Number($("#premium").val() / 100);
		
	let total = calculateTotalBidCost(currentBid, vatRate, buyersPremium, vatApplicable);
	
	$("span#total").text(total.toFixed(2));
	
}



function calculateTotalBidCost(bidAmount, vatRate, buyersPremium, vatApplicable) {
	
	let temp_totalVat = Number(bidAmount) * vatRate;
	let temp_premium = Number(bidAmount) * buyersPremium;
	let temp_premiumVAT =  (vatApplicable) ? (temp_premium * vatRate) : 0;
	
	let totalCost = temp_totalVat + temp_premium + temp_premiumVAT + bidAmount;
	return totalCost;
}

