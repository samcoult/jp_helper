


 
'use strict';

$(document).ready(function() {

	var alarmName = 'remindme';

	function checkAlarm(callback) {

	 chrome.alarms.getAll(function(alarms) {
	   var hasAlarm = alarms.some(function(a) {
		 return a.name == alarmName;
	   });
	   var newLabel;
	   if (hasAlarm) {
		 newLabel = 'Cancel alarm';
	   } else {
		 newLabel = 'Activate alarm';
	   }
	   $('#toggleAlarm span.buttonText').innerText = newLabel;
	   if (callback) callback(hasAlarm);
	 })
	}


	function createAlarm() {
		
		let delayInMinutes = 0.1;
		
		console.log("Creating alarm for " + delayInMinutes + " minutes");
		chrome.alarms.create(alarmName, 
			{
				delayInMinutes: delayInMinutes, 
				periodInMinutes: delayInMinutes
			}
		);
	}


	function cancelAlarm() {
		console.log('cancelling alarm');
	 chrome.alarms.clear(alarmName);
	}


	function doToggleAlarm() {
	 checkAlarm( function(hasAlarm) {
	   if (hasAlarm) {
		 cancelAlarm();
	   } else {
		 createAlarm();
	   }
	   checkAlarm();
	 });
	}


	$('#toggleAlarm').on('click', doToggleAlarm);
	checkAlarm();


});